#include "../include/bmp.h"
#include "../include/image.h"

struct __attribute__((packed))bmp_header {
    uint16_t bfType; // specifies the file type 0x4D42
    uint32_t bfileSize; // specifies the size in bytes of the bitmap file
    uint32_t bfReserved; // reserved; must be 0
    uint32_t bOffBits; // specifies the offset in bytes from the bitmapfileheader to the bitmap bits
    uint32_t biSize; // specifies the number of bytes required by the struct 40(BITMAPINFOHEADER) или 108(BITMAPV4HEADER) или 124(BITMAPV5HEADER)
    uint32_t biWidth; //specifies width in pixels
    uint32_t biHeight; //specifies height in pixels
    uint16_t biPlanes; //specifies the number of color planes, must be 1
    uint16_t biBitCount; // specifies the number of bits per pixel 0 | 1 | 4 | 8 | 16 | 24 | 32
    uint32_t biCompression; // specifies the type of compression BI_RGB | BI_RLE8 | BI_RLE4 | BI_BITFIELDS | BI_JPEG | BI_PNG
    uint32_t biSizeImage; // size of image in bytes (обычно 0)
    uint32_t biXPelsPerMeter; // number of pixels per meter in x axis
    uint32_t biYPelsPerMeter; // number of pixels per meter in y axis
    uint32_t biClrUsed; // number of colors used by the bitmap
    uint32_t biClrImportant; // number of colors that are important 0
};

struct bmp_header create_bmp(const uint64_t width, const uint64_t height) {
    const struct bmp_header bmp_header = {
            .bfType = 0x4D42,
            .bfileSize =  sizeof(struct bmp_header) + width * height * sizeof(struct pixel) +
                          height * (width % 4), // размер файла
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),// смещение до поля данных,
            .biSize = 40, // размер струкуры в байтах:
            .biWidth = width, // ширина в точках
            .biHeight = height,  // высота в точках
            .biPlanes = 1, // всегда должно быть 1
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage =  height * width * sizeof(struct pixel) +
                            (width % 4) * height, // Количество байт в поле данных
            .biXPelsPerMeter = 0, // горизонтальное разрешение, точек на дюйм
            .biYPelsPerMeter = 0, // вертикальное разрешение, точек на дюйм
            .biClrUsed = 0, // Количество используемых цветов
            .biClrImportant = 0, // Количество существенных цветов.
    };
    return bmp_header;
}

static uint8_t get_padding(uint32_t width) {
    return width % 4;
}

static enum read_status check_header(struct bmp_header *const header) {
    if (header->bfType != 0x4d42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biSize != 40) {
        return READ_INVALID_HEADER;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}


enum read_status from_bmp(FILE *in, struct image *img) {
    if (!in || !img) {
        return READ_ERROR;
    }
    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) == 1) {
        if (check_header(&header) != READ_OK) {
            return check_header(&header);
        }
    } else {
        return READ_INVALID_PLANES;
    }

    *img = init_img(header.biWidth, header.biHeight);

    for (size_t i = 0; i < img->height; i++) {
        if (fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, in) != img->width) {
            free_img(img);
            return READ_ERROR;
        }
        if (fseek(in, get_padding(img->width), SEEK_CUR) != 0) {
            free_img(img);
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    const struct bmp_header bmp_header_former = create_bmp(img->width, img->height);
    if (fwrite(&bmp_header_former, sizeof(struct bmp_header), 1, out) == 1) {
        const uint64_t fill = 0;
        for (uint64_t i = 0; i < img->height; i++) {
            if (fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out) != img->width) {
                return WRITE_ERROR;
            }
            if (fwrite(&fill, 1, get_padding(img->width), out) != get_padding(img->width)) {
                return WRITE_ERROR;
            }
        }
        return WRITE_OK;
    }
    return WRITE_ERROR;
}
