#include "rotation.h"
#include "image.h"
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if (argc != 3) {
        fprintf(stderr, "Number of arguments is not correct");
        return 1;
    }

    (void) argc; (void) argv;
    FILE * f = open_for_read(argv[1]);
    if (f == NULL){
        fprintf(stderr, "No file to read");
        return 1;
    }

    struct image i = create_img();

    if(from_bmp(f, &i) != READ_OK){
        close(&f);
        fprintf(stderr, "Problem with reading");
        return 1;
    }
    if (close(&f)!=0){
        fprintf(stderr, "Problem with closing");
        return 1;
    }
    struct image i2= rotation(&i);

    FILE *f_w = open_for_write(argv[2]);
    if (f_w == NULL){
        fprintf(stderr, "No file to write");
        return 1;
    }

    if(to_bmp(f_w, &i2)!=WRITE_OK){
        close(&f_w);
        fprintf(stderr, "Problem with writing");
        return 1;
    }
    if (close(&f_w)!=0){
        fprintf(stderr, "Problem with closing");
        return 1;
    }
    free_img(&i);
    free_img(&i2);
    return 0;
}
