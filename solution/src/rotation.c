#include "rotation.h"

static struct pixel get_pixel(struct image const* source, uint64_t i, uint64_t j){
    return source->data[(source->height-j-1)*source->width+i];
}

struct image rotation (struct image const *source) {
    struct image new = init_img(source->height, source->width);

    for (uint64_t i = 0; i < source->width; i++) {
        for (uint64_t j = 0; j < source->height; j++) {
            new.data[i*new.width+j] = get_pixel(source, i, j);
        }
    }
    return new;
}
