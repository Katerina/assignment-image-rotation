#pragma once
#include "image.h"
#include "sourcefile.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PLANES,
    READ_ERROR
    /* коды других ошибок  */
};

struct bmp_header create_bmp (const uint64_t width, const uint64_t height) ;
enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );
