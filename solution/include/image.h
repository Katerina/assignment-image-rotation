#pragma once
#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_img ();

struct image init_img (uint64_t width, uint64_t height);

void free_img (struct image* img);
